package messages

import "testing"

// TestMessage returns a hello world message
func TestMessage(t *testing.T) {
	if message := Message(); message != "hello world" {
		t.Error("Message was altered")
	}
}
